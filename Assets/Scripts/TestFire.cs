﻿using UnityEngine;
using System.Collections;

public class TestFire : MonoBehaviour {

	public Transform cannon_spawn;  			//Tracks the direction, rotation of the cannon
	public GameObject player_ball_spawn;		//Dummy object for the player spawn
	public float speed;							//Set to correct speed/direction for each cannon
	public string fire_button = "fire";
	public float fireRate = 0.5F;				//Delay on firing
	private float nextFire = 0.0F; 				//Set initial fire delay to zero

	//Start the timer (with no delay yet)

	public GameObject player_ball_blue;
	public GameObject player_ball_green;
	public GameObject player_ball_yellow;
	public GameObject player_ball_purple;

	int counter;


	void FixedUpdate () {
		int rand = 0;
	
		//Fire only when space is pressed and enough time has passed
		if (Input.GetButtonDown (fire_button) && Time.time > nextFire) {

			//Add delay to current time, cannon cannot fire again until that time has passed
			nextFire = Time.time + fireRate;

            rand = Random.Range(0, 5);

			if (rand == 0) {
				//TODO always summons a red ball -Alec
				GameObject player_ball = Instantiate (player_ball_spawn, cannon_spawn.position, cannon_spawn.rotation) as GameObject;

				//Physics for the ball, does not work if you create a RigidBody instead of a GameObject for some reason
				Rigidbody rb = player_ball.GetComponent<Rigidbody> ();

				//Fire and multiply by the correct direction (positive or negative)
				rb.velocity = cannon_spawn.transform.up * speed;
			}

			if (rand == 1) {
				//TODO always summons a red ball -Alec
				GameObject player_ball = Instantiate (player_ball_blue, cannon_spawn.position, cannon_spawn.rotation) as GameObject;

				//Physics for the ball, does not work if you create a RigidBody instead of a GameObject for some reason
				Rigidbody rb = player_ball.GetComponent<Rigidbody> ();

				//Fire and multiply by the correct direction (positive or negative)
				rb.velocity = cannon_spawn.transform.up * speed;
			}

			if (rand == 2) {
				//TODO always summons a red ball -Alec
				GameObject player_ball = Instantiate (player_ball_green, cannon_spawn.position, cannon_spawn.rotation) as GameObject;

				//Physics for the ball, does not work if you create a RigidBody instead of a GameObject for some reason
				Rigidbody rb = player_ball.GetComponent<Rigidbody> ();

				//Fire and multiply by the correct direction (positive or negative)
				rb.velocity = cannon_spawn.transform.up * speed;
			}

			if (rand == 3) {
				//TODO always summons a red ball -Alec
				GameObject player_ball = Instantiate (player_ball_green, cannon_spawn.position, cannon_spawn.rotation) as GameObject;

				//Physics for the ball, does not work if you create a RigidBody instead of a GameObject for some reason
				Rigidbody rb = player_ball.GetComponent<Rigidbody> ();

				//Fire and multiply by the correct direction (positive or negative)
				rb.velocity = cannon_spawn.transform.up * speed;
			}

			if (rand == 4) {
				//TODO always summons a red ball -Alec
				GameObject player_ball = Instantiate (player_ball_yellow, cannon_spawn.position, cannon_spawn.rotation) as GameObject;

				//Physics for the ball, does not work if you create a RigidBody instead of a GameObject for some reason
				Rigidbody rb = player_ball.GetComponent<Rigidbody> ();

				//Fire and multiply by the correct direction (positive or negative)
				rb.velocity = cannon_spawn.transform.up * speed;
			}
		
		}
	}



}
