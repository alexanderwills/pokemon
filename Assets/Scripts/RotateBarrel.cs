﻿using UnityEngine;
using System.Collections;

public class RotateBarrel : MonoBehaviour {

	public float speed = 2;
	public string axis = "Vertical";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
		float v = Input.GetAxisRaw(axis);

		//Rotate if barrel is facing the game board
		if (gameObject.transform.localEulerAngles.z > 10 && gameObject.transform.localEulerAngles.z < 170) {
		
			transform.Rotate(0, 0, v * speed);
		}
		//If barrel is facing the bottom only allow player to rotate towards the board
		else if (gameObject.transform.localEulerAngles.z >= 170 && v < 0) {

			transform.Rotate (0, 0, v * speed);

		} 
		//If barrel is facing the top only allow player to rotate towards the board
		else if (gameObject.transform.localEulerAngles.z <= 10 && v > 0) {

			transform.Rotate (0, 0, v * speed);

		}


	}
}
