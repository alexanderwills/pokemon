﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update() {

		//If user presses ESC give up
		if(Input.GetKeyDown(KeyCode.Escape)){

			SceneManager.LoadSceneAsync("final_score");

		}

	}
}
