﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class load_level : MonoBehaviour {

	public void LoadSelectedLevel(int level_number){

		switch (level_number) {

		case 0:
			SceneManager.LoadSceneAsync ("menu_scene");
			break;
		case (1):
			SceneManager.LoadSceneAsync("main_scene");
			break;
		case (2):
			SceneManager.LoadSceneAsync("level_2");
			break;
		case (3):
			SceneManager.LoadSceneAsync("level_3");
			break;
		default:
			SceneManager.LoadSceneAsync("final_score");
			break;

		}
		//Debug.Log ("Level " + level_number);

	}

}
