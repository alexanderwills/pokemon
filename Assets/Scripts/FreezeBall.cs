﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

//This was created with ana's frustrations

public class FreezeBall : MonoBehaviour {

	int counter = 0;

	public List<GameObject> OrbsTouching;
    
    public string Color;
    public bool Visited;

    public static List<string> BallTags = new List<string> {"Purple", "Red", "Blue", "Yellow", "Green"};

    void Start()
    {
        this.OrbsTouching = new List<GameObject>();
        this.Color = this.gameObject.tag;
        this.Visited = false;
        List<Collider> nearbyColliders = null;

        if (this.gameObject.GetComponent<SphereCollider>())
        {
            nearbyColliders = (Physics.OverlapSphere(this.gameObject.transform.position, this.gameObject.GetComponent<SphereCollider>().radius)).ToList();
        }
        else
        {
            Debug.Log("Failed to get Sphere collider on ball: " + this.gameObject);
        }

        if (nearbyColliders != null && nearbyColliders.Any())
        {
            nearbyColliders.RemoveAll(x => !(BallTags.Contains(x.gameObject.tag)) );
            foreach (Collider nearbyCollider in nearbyColliders)
            {
                if (!nearbyCollider.gameObject.Equals(this.gameObject))
                {
                    OrbsTouching.Add(nearbyCollider.gameObject);
                }
            }
        }
    }

    void Update()
    {
        this.OrbsTouching.RemoveAll(elem => elem == null);
        this.OrbsTouching.RemoveAll(x => x.gameObject.Equals(this.gameObject));
        List<GameObject> likeColoredOrbs = OrbsTouching.Where(orb => orb.tag == this.Color).ToList();
        if (likeColoredOrbs.Capacity > 0 )
        {
            this.Visited = true;
            int numVisited = 1; // Self

            Debug.Log(this.Color + " , " + this.gameObject + " was Triggered.");

            foreach (var coloredOrb in likeColoredOrbs)
            {
                numVisited = TryPoppingDFS(numVisited, coloredOrb);
            }

            foreach (var coloredOrb in likeColoredOrbs)
            {
                UnflagAndTryDestroyingDFS(numVisited, coloredOrb);
            }
        }
    }

    public void UnflagAndTryDestroyingDFS(int numVisited, GameObject currentOrb)
    {
        this.Visited = false;
        currentOrb.gameObject.GetComponent<FreezeBall>().UnflagAndTryDestroySelf(numVisited);
        if (numVisited > 2)
        {
            Destroy(this.gameObject);
        }
    }

    public void UnflagAndTryDestroySelf(int numVisited)
    {
        List<GameObject> likeColoredOrbs = this.OrbsTouching.Where(orb => orb.tag == this.Color).ToList();
        this.Visited = false;

        foreach (var coloredOrb in likeColoredOrbs)
        {
            if (coloredOrb.gameObject.GetComponent<FreezeBall>().Visited == true)
            {
                coloredOrb.gameObject.GetComponent<FreezeBall>().UnflagAndTryDestroySelf(numVisited);
            }
        }
        
        if (numVisited > 2)
        {
            Destroy(this.gameObject);
        }
    }

    public int TryPoppingDFS(int currentNumVisited, GameObject currentOrb)
    {
        int numVisited = currentOrb.GetComponent<FreezeBall>().TraverseSelf(currentNumVisited);
        return numVisited;
    }

    public int TraverseSelf(int numVisited)
    {
        List<GameObject> likeColoredOrbs = this.OrbsTouching.Where(orb => orb.tag == this.Color).ToList();
        this.Visited = true;
        numVisited++;

        Debug.Log("Traversing: " + this.gameObject + " , " + this.Color);

        foreach (var coloredOrb in likeColoredOrbs)
        {
            if (coloredOrb.gameObject.GetComponent<FreezeBall>().Visited == false)
            {
                numVisited = coloredOrb.gameObject.GetComponent<FreezeBall>().TraverseSelf(numVisited);
            }
        }

        return numVisited;
    }

	void OnCollisionEnter(Collision col)
    {
        col.rigidbody.constraints = RigidbodyConstraints.FreezeAll;
	    if (!OrbsTouching.Contains(col.gameObject))
	    {
	        this.OrbsTouching.Add(col.gameObject);
	    }
    }
}

