﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScoreTrackingScript : MonoBehaviour {

	//Track the score of the current game
	public int score = 0;

	// Use this for initialization
	void Start () {

		//Keep this object around when switching scenes
		DontDestroyOnLoad(this);

	}



	//This kills the GameObject
	void destroy() {

		Destroy (gameObject);

	}


}
